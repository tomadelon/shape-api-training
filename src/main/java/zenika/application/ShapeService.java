package zenika.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zenika.DTO.ColorDTO;
import zenika.DTO.ShapeDTO;
import zenika.restservice.ColorService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Component
public class ShapeService {

    private final List<Shape> shapeList = Arrays.asList(Shape.values());
    private final int listSize = shapeList.size();

    private ColorService colorService;
    private Random random;

    @Autowired
    public ShapeService(ColorService colorService, Random random){
        this.colorService = colorService;
        this.random = random;
    }

    public ShapeDTO getRandomShape() {
        ColorDTO randomColor = colorService.getRandomColorFromApi();
        Shape randomShape = shapeList.get(random.nextInt(listSize));
        return  new ShapeDTO(randomShape,
                randomColor.getRed(),
                randomColor.getGreen(),
                randomColor.getBlue());
    }
}
